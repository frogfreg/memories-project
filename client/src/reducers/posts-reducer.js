export const postsReducer = (posts, action) => {
  switch (action.type) {
    case "fetch-all":
      return action.payload;
    case "create":
      return [...posts, action.payload];
    case "update":
      console.log(action.payload);
      return posts.map((post) => {
        if (post._id === action.payload._id) {
          return action.payload;
        } else {
          return post;
        }
      });
    case "delete":
      return posts.filter((post) => {
        return post._id !== action.payload;
      });
    case "like":
      console.log(action.payload);
      return posts.map((post) => {
        if (post._id === action.payload._id) {
          return action.payload;
        } else {
          return post;
        }
      });
    default:
      throw new Error();
  }
};
