import { useContext } from "react";
import { Grid, CircularProgress } from "@material-ui/core";
import StoreContext from "../../context/store-context.js";
import Post from "./post/post.js";
import useStyles from "./styles.js";

function Posts() {
  const classes = useStyles();
  const { posts } = useContext(StoreContext);
  return !posts.length ? (
    <CircularProgress />
  ) : (
    <Grid
      className={classes.container}
      container
      alignItems="stretch"
      spacing={3}
    >
      {posts.map((post) => {
        return (
          <Grid item key={post._id} xs={12} sm={6}>
            <Post post={post} />
          </Grid>
        );
      })}
    </Grid>
  );
}

export default Posts;
