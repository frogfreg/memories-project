import { useState, useContext, useEffect } from "react";
import useStyles from "./styles.js";
import { TextField, Button, Typography, Paper } from "@material-ui/core";
import FileBase from "react-file-base64";
import StoreContext from "../../context/store-context.js";
import { createPost, updatePost } from "../../api/index.js";

function Form() {
  const classes = useStyles();
  const { dispatch, currentId, setCurrentId, posts } = useContext(StoreContext);
  const selectedPost = currentId
    ? posts.find((post) => {
        return post._id === currentId;
      })
    : null;
  const [postData, setPostData] = useState({
    creator: "",
    title: "",
    message: "",
    tags: "",
    selectedFile: "",
  });

  useEffect(() => {
    if (selectedPost) {
      setPostData(selectedPost);
    }
  }, [selectedPost]);

  async function handleSubmit(event) {
    event.preventDefault();

    if (currentId) {
      try {
        const { data } = await updatePost(currentId, postData);
        dispatch({ type: "update", payload: data });
      } catch (err) {
        console.error(err);
      }
    } else {
      try {
        const { data } = await createPost(postData);
        dispatch({ type: "create", payload: data });
      } catch (err) {
        console.error(err);
      }
    }
    clear();
  }

  function clear() {
    setCurrentId(null);
    setPostData({
      creator: "",
      title: "",
      message: "",
      tags: "",
      selectedFile: "",
    });
  }
  return (
    <Paper className={classes.paper}>
      <form
        autoComplete="off"
        noValidate
        className={`${classes.form} ${classes.root} ${classes.form}`}
        onSubmit={handleSubmit}
      >
        <Typography variant="h6">
          {currentId ? "Editing a memory" : "Creating a memory"}
        </Typography>
        <TextField
          value={postData.creator}
          onChange={(event) => {
            setPostData({ ...postData, creator: event.target.value });
          }}
          name="creator"
          variant="outlined"
          label="Creator"
          fullWidth
        />
        <TextField
          value={postData.title}
          onChange={(event) => {
            setPostData({ ...postData, title: event.target.value });
          }}
          name="title"
          variant="outlined"
          label="Title"
          fullWidth
        />
        <TextField
          value={postData.message}
          onChange={(event) => {
            setPostData({ ...postData, message: event.target.value });
          }}
          name="message"
          variant="outlined"
          label="Message"
          fullWidth
        />
        <TextField
          value={postData.tags}
          onChange={(event) => {
            setPostData({ ...postData, tags: event.target.value.split(", ") });
          }}
          name="tags"
          variant="outlined"
          label="Tags"
          fullWidth
        />
        <div className={classes.fileInput}>
          <FileBase
            type="file"
            multiple={false}
            onDone={({ base64 }) => {
              setPostData({ ...postData, selectedFile: base64 });
            }}
          />
        </div>
        <Button
          className={classes.buttonSubmit}
          variant="contained"
          color="primary"
          size="large"
          type="submit"
          fullWidth
        >
          Submit
        </Button>
        <Button
          variant="contained"
          color="secondary"
          size="small"
          onClick={clear}
          fullWidth
        >
          Clear
        </Button>
      </form>
    </Paper>
  );
}

export default Form;
