import { useState, useReducer, useEffect } from "react";
import { Container, AppBar, Typography, Grow, Grid } from "@material-ui/core";
import Posts from "./components/posts/posts.js";
import Form from "./components/form/form.js";
import useStyles from "./styles.js";
import StoreContext from "./context/store-context.js";
import { postsReducer } from "./reducers/posts-reducer.js";
import { fetchPosts } from "./api/index.js";

function App() {
  const classes = useStyles();
  const [posts, dispatch] = useReducer(postsReducer, []);
  const [currentId, setCurrentId] = useState(null);

  useEffect(() => {
    const getPosts = async () => {
      try {
        const response = await fetchPosts();
        dispatch({ type: "fetch-all", payload: response.data });
      } catch (err) {
        console.error(err);
      }
    };
    console.log("Call to useEffect!");
    getPosts();
  }, [currentId]);

  return (
    <StoreContext.Provider value={{ posts, dispatch, currentId, setCurrentId }}>
      <Container maxWidth="lg">
        <AppBar className={classes.appBar} position="static" color="inherit">
          <Typography className={classes.heading} variant="h2" align="center">
            Memories
          </Typography>
          <img
            className={classes.image}
            src="https://raw.githubusercontent.com/adrianhajdin/project_mern_memories/master/client/src/images/memories.png?token=AF56X74XONEUGZ4FD2FUIA27UURPI"
            alt="memories"
            height="60"
          />
        </AppBar>
        <Grow in>
          <Container>
            <Grid
              className={classes.mainContainer}
              container
              justify="space-between"
              alignItems="stretch"
              spacing={3}
            >
              <Grid item xs={12} sm={7}>
                <Posts />
              </Grid>
              <Grid item xs={12} sm={4}>
                <Form />
              </Grid>
            </Grid>
          </Container>
        </Grow>
      </Container>
    </StoreContext.Provider>
  );
}

export default App;
